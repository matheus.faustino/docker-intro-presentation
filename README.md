# docker-intro-presentation

Introduction of docker with examples

## Commands
- `docker build -t matheus.faustino/docker-intro-presentation:0.4.0 .`
- `docker run -it -p 80:80 -v ${PWD}/api:/var/www/html --rm "matheus.faustino/docker-intro-presentation:0.4.0"`

### Extra
- `docker run -e APP_VERSION="0.1.1" -p 80:80 --rm "matheus.faustino/docker-intro-presentation:0.2.0"`
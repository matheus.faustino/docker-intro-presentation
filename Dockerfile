FROM php:7.4-apache

# default dir that container will operates (commands will be run from this folder)
WORKDIR /var/www/html

# publish a PORT from container
EXPOSE 80

# Change default folder apache
ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf

# Fix apache permission with var folder
RUN usermod -u 1000 www-data

VOLUME /var/www/html

# entrypoint
ENTRYPOINT ["docker-php-entrypoint"]

# cmd
CMD ["apache2-foreground"]
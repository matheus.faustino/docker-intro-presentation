<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DockerController extends AbstractController
{
    /**
     * @Route("/docker", name="docker")
     */
    public function index(): Response
    {
        return new Response(<<<EOF
        <html>
            <head><title>Test</title></head>
            <body>
                <img src=" http://via.placeholder.com/500" />
            </body>
        </html>
EOF
        );
    }
}